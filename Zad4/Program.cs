﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            FileLogger fileLogger = new FileLogger("E:\\systemData.txt");
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemDataProvider.Attach(fileLogger);
            systemDataProvider.Attach(consoleLogger);
            while (1 != 0)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
