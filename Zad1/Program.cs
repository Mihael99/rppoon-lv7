﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 5.6, 1.8, 22.2, 15.3, 1.1, 7.13, 13.9, 0.56, 4.7, 23.06 };
            NumberSequence sequence = new NumberSequence(array);
            BubbleSort bubbleSort = new BubbleSort();
            CombSort combSort = new CombSort();
            SequentialSort sequentialSort = new SequentialSort();

            Console.WriteLine("Bubble sort:");
            sequence.SetSortStrategy(bubbleSort);
            sequence.Sort();
            Console.WriteLine(sequence.ToString());

            Console.WriteLine("Comb sort:");
            sequence.SetSortStrategy(combSort);
            sequence.Sort();
            Console.WriteLine(sequence.ToString());

            Console.WriteLine("Sequential sort:");
            sequence.SetSortStrategy(sequentialSort);
            sequence.Sort();
            Console.WriteLine(sequence.ToString());

        }
    }
}
