﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class LinearSearch: SearchStrategy
    {
        public override void Search(double[] array, double number)
        {
            int found = 0;
            int place = 0;
            for(int i=0; i<array.Length; i++)
            {
                if (number == array[i])
                {
                    found = 1;
                    place = i;
                }
            }
            if(found == 1)
            {
                Console.WriteLine("Broj pronadjen na " + place + ". mjestu.");
            }
            else
            {
                Console.WriteLine("Broj nije pronadjen.");
            }
        }
    }
}
