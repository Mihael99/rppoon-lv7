﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    abstract class SearchStrategy
    {
        public abstract void Search(double[] array, double number);
    }
}
