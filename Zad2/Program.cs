﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 5.6, 1.8, 22.2, 15, 1.1, 7, 13.9, 0.56, 4.7, 23.06, 13, 7.7, 26, 4.4 };
            NumberSequence sequence = new NumberSequence(array);
            LinearSearch linearSearch = new LinearSearch();
            sequence.SetSearchStrategy(linearSearch);
            sequence.Search(4.7);
            sequence.Search(11);
        }
    }
}
