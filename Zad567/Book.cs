﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    //zadatak 5.
    class Book: IItem
    {
        public string title { get; private set; }
        public double price { get; private set; }
        public Book(string Title, double Price)
        {
            this.title = Title;
            this.price = Price;
        }
        public override string ToString()
        {
            return "Book: " + this.title + " " + Environment.NewLine + "Price: " + this.price;
        }
        public double Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
