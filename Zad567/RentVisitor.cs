﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    //Zadatak 6.
    class RentVisitor: IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            //Zadatak 6.
            //if(DVDItem.Type == DVDType.SOFTWARE)
            //{
            //    return double.NaN;
            //}
            return DVDItem.Price * 0.10;
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * 0.10;
        }
        public double Visit(Book BookItem)
        {
            return BookItem.price * 0.10;
        }
    }
}
