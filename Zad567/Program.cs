﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 5.
            Console.WriteLine("Buy visitor:");
            DVD dvd1 = new DVD("Lord Of The Rings", DVDType.MOVIE, 9.99);
            VHS vhs1 = new VHS("Titanic", 4.99);
            Book book1 = new Book("Game Of Thrones", 29.99);
            BuyVisitor visitor1 = new BuyVisitor();
            Console.WriteLine(dvd1.Accept(visitor1));
            Console.WriteLine(vhs1.Accept(visitor1));
            Console.WriteLine(book1.Accept(visitor1));

            //Zadatak 6.
            Console.WriteLine("Rent visitor:");
            DVD dvd2 = new DVD("Windows Vista", DVDType.SOFTWARE, 4.99);
            RentVisitor rentVisitor1 = new RentVisitor();
            Console.WriteLine(dvd1.Accept(rentVisitor1));
            Console.WriteLine(dvd2.Accept(rentVisitor1));
            Console.WriteLine(vhs1.Accept(rentVisitor1));
            Console.WriteLine(book1.Accept(rentVisitor1));

            //Zadatak 7.
            Console.WriteLine("Buying with a cart.");
            Cart cartOne = new Cart();
            cartOne.AddItemToCart(dvd1);
            cartOne.AddItemToCart(book1);
            cartOne.AddItemToCart(vhs1);
            Console.WriteLine(cartOne.Accept(visitor1));
        }
    }
}
