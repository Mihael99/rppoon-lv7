﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    //Zadatak 7.
    class Cart
    {
        private List<IItem> cart;
        public Cart()
        {
            cart = new List<IItem>();
        }
        public void AddItemToCart(IItem item)
        {
            cart.Add(item);
        }
        public void RemoveItemFromCart(IItem item)
        {
            cart.Remove(item);
        }
        public double Accept(IVisitor visitor)
        {
            double sumOfPrices = 0;
            foreach(IItem item in cart)
            {
                sumOfPrices+=item.Accept(visitor);
            }
            return sumOfPrices;
        }
    }
}
